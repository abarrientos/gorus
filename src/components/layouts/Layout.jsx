import React from 'react';
import { useLocation } from 'react-router-dom';
import useAuth from '../../auth/useAuth';
import Navigation from '../Navigation';
import { useEffect } from 'react';
import { getData } from '../../helpers/AsyncStorageF';
import ResumenPerfil from '../ResumenPerfil';
// import "../../styles/sidebar.scss"

const Layout = ({ children }) => {
    const { login, isLogged } = useAuth();
    const location = useLocation()

    useEffect(async () => {
        const data = await getData('GORUS_SESSION_INFO');
        if (data) {
            login(data, location.state?.from)
        }
    }, []);

    const styleMain = {
        marginLeft: isLogged() ? '560px' : '0px',
        transition: 'margin-left 0.5s'
    }
    return (
        <>
            <div>
                <Navigation />
                {
                    isLogged() &&
                    <div>
                        <ResumenPerfil />
                    </div>
                }
                <div id="main" style={styleMain}>
                    {children}
                </div>
            </div>
        </>
    )
}

export default Layout;
