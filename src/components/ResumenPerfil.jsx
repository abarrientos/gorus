import React, { useEffect, useState } from 'react'
import useAuth from '../auth/useAuth'
import SolicitarRetiroModal from './SolicitarRetiroModal'

export default function ResumenPerfil () {
    const { isLogged } = useAuth()

    const openNav = () => {
        document.getElementById("myResumenNav").style.width = "300px"
    }

    useEffect(() => {
        if (isLogged()) {
            openNav()
        }
    },[isLogged()])

    const cardContent = {
        backgroundColor: '#000',
        color: '#FFF',
        padding: '9px',
        borderRadius: '8px',
        boxShadow: 'rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px'
    }


    return (
        <div className='sideResumen' id='myResumenNav' style={{ backgroundColor: 'white' }}>
            <div className='text-center text-bold' style={{ paddingTop: '15px', fontSize: '20px' }}>
                Resumen del perfil
            </div>

            <section style={{ padding: '10px', borderRadius: '15px' }}>
                <div style={cardContent}>
                    <div className='row'>
                        <div className='col-3' >
                            <Avatar />
                        </div>
                        <div className='col-9 row'>
                            <div className='col-12 text-right'>Ingresos del mes</div>
                            <div className='col-12 text-right text-bold'>$26.00</div>
                            <br/> <br/>
                            <div className='col-12 text-right'>Saldo disponible</div>
                            <div className='col-12 text-right text-bold'>$10.000</div>
                        </div>
                    </div>
                </div>
            </section>

            <BtnRetiro />

            <LastChat />
        </div>
    )
}

const Avatar = () => {
    const contentStyle = {
        alignContent: 'flex-start',
        alignItems: 'start',
        display: 'flex',
        justifyContent: 'flex-start',
    }
    return (
        <div style={contentStyle}>
            <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQoAAAC9CAMAAAB8rcOCAAAB71BMVEUhvY7raVJusaCzs7NPptv6rRfim5cAAAAhwJB+Rt0iwpIixZRwtKO2trYewZDmnppzuaf2blaokXL/sBSCSOTuo59SquKgmJsdw44auIlloJFVhXkANiT/tRgdpn1KcWgSh2QLAAAAWUAAkWsUgWEAmnAAIxUArIEACAAaely6enmEQ+Y9fWqnoqRqqZkAlXQ2TklLmM/4aVIwAAAHZkpDZl4qAAB2eXgAQTFMem5qcmKNb2h5cWSEezadeyU5bFXbi4tpXVSJaR3/qQB2ZylefXKYj5OFfYBFUUQpdl9iVyRvb29FUIZJTYs4aVsAkHLomgAPWUcmWl2paGkIQDlQflAdGAlaLZxoKbMJg1dbRUJ1PcwARB5TcF3KgQCeZxipRUC9T0aGLCVTP5YnbmlORkk9MDVYPomIROwPq3UAFgCdoJdlNrGATk+jUDRMKoVcLzOJWxEhADI1ZHZCRD8/WUPOWEVSD4puJrwHl2NDAG9sPL5TZFweAAAAKAAAZT0AOxAwAE+YZ2RTMQAAGx4bPUAtAEoGcEqzdwA8b0qTTz19PG+KQGNaMZdtPEC9dQBGFQAdHB+AUj8vdYhwJBhwXEY1cJMqc35kZURhKyEfN1MtU3lAGxUSFy7RTDwkRWFqf0drPQAXPUVBfrAWIji56NPHAAAMRUlEQVR4nO2bj1cbVRbHk1CZx7zJZNIQyCSQTDITpuFXgYIkdhVcWmisIhR2KdZW6C+birZaTVu1dkW7xe7WQi20WmV/4B+679dMJtBq3WO147mfc/QcCcTc79z7ffe+9xIIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMCzCFIJj39VlmX0G36a3xNsvrD/T9ZjtbBefOlFQ37C9yKa2ratqv7UDo+MxiORyMsqzY4dIdt/HovFYmMHnuitkF44ODHeOj5x0CJioETi8bn2LIKM0UgjIX4Im92lsl7/qnyg/TlK+ytP8pz1w2FBw3A+Yb76mpV4Kp/5KaHuZ0o0RjpzSUVRJuvyAr0+9hxnavDna0Qthxscwq3TXc3NXUfwU/vgvz7qDJeisXFWC4VCvQXv45f/EhNStP/154NSSzUpGsJzzU1NzUfnfeQa6rKQIv6GQqRQOrwhyy9xKci/923/u8QOK0BWQ7iB1wfhaBPh2JtParjPAOrxuMiKxiGSFsk2rxT6CSZF7K2FxZPpuqAS1mtzp04n6p+5OtTa0Hr4zNkJokX47S6SFXN+kiJgzwgtIp0pTUnlPJ9dWEXsrWAwGF2wPXEnXiVOsHRurrJNi8Hz53NW2hwn2fHOHPkNKe+jAgmg9LujcbKcci2GLM9nF1YRey9ItSjWREoc6aLZf+Hc7e0rDsYyQribpsU7e6Tzpp+SgnYDOSl7caYx0hgZzdxIk+bSeUVYhZBCqpVOhRlBU9OFY2+6P0TEPURrhUxmnGfS2E85wSDPUje/JP4ZibxbMAuWLeITVtH+PiuQmougq11ciualDyrCL9TKpVOnLlW4k+rjNC0mLD8tpS4okEp+SJqtSHy0c6azzGJwrOKE9FG1elJKu4/YlaKpuevokUqCNNrYunyM9BFXDPaX6jBbRIbL/mo3OSifVLShUWafxDdGaETcKmJT0l5sTOe8rlm5woX4ZG7p2Lkrpy4ZV+c//qSZ/uQKayREsxVumPaXVzCQQboK7VPRY0RmTORYRexaAdPxlCmB+KSVuMRt89jtz67/7fOj5851dTWLNPmcGilKt3ItJgzfuUVALdAGK+T0W43UGJhVtH9RrFW8bHV3sxk28TZZTD/pWpLSGKvzbZebmp2KmeNpkeFStBb9lxZqN5VC+9BJC7JcMKtof/9LEQxJDFwOKUqIFg9K3Li8NPd3KU9fRLhy/baTFXM32e+TyYxpIfnPOdUSk+KNzgjvtkj/Ta2i/S1J5ymOX39xZeVAivxWL0t6nL950wiI9EeJVy8c41Jc4I0J0vuoFuGz9u8V0f+JjHUaZKhX4lLELxZUahXtK5JwS/mrqRhhqqSFtFn23JHq2dxCZteFpuZmkhrOHyCb2kX4ZdNfZoELtxbuprQQabvNzngk3vj1HhKAPdV+wllBkTHGu62plKI9KulJ/7n0j3/ebvogJ15EZg/zzScY7p8VEOmTDw1Eg9HNIU3JFLB59uuLUpG+ot9Z4V4QqI2oz8UOKL0dj6r/xHQ1Go2uuiOMOsEKpLXNL2aBcNpMGwO0sQ5udmt9JhEmndd5h7V3lkyjZPkMYGSfcKRYERMb6bPrdjDx3Sh5j2rBMdl8D19N/SIFsu9uDgRPRpkU0VVtjZocciJEMpKx+WBx865dk+KOkqHGKJvfbJy+6tmxwAvsXZyMQQZLih6p4BOv0NnnXwhyKe4l610AYbv8XpCkTHRRX3GkeEnrI4M3MvpbWlr6NwoVJzXUdfqL1ZoUfC3N+SQp1JGBmhIkK1KebCYpYaxvDvCEGShbQoqxkrZG+snERssuQkvL1v15XijIXohGq1lnxucFEj5j+yQp8C1W30KKoFTbt5Fx+vii0IFK8QC/QtfS2NgDRaGpo27tEpDU6L5Kp1Nktz0vFVwt+XB62PKVFDQhCMF9mYxoj7Bu3docCNYYIG740crU1EpJUdiWn8gKR41vLF0lhotqWx18OA235nyylsrHWbzVhW9X792T+pgLBGTVXK/WEoIpcYvUBDZKSdJ2KynaKaD8lkcLWiin8/W7vurBsK8ab7tKQ45WpcneZK+mZWnEhYWgNyFIxix+Syse5XvZlvgQ67vl+e+2WurUIB7q1ULt5lL8XqH9UuQ88broolRIKTRM8gjxSH1CRKsn2wzuixabXbU+3oCiwHzbw/761Dji0QKXuBT6Y/7XzxyyXXxe6jDtJAlTSRZlZFa9OgQXvh+0xQalWuZSZN22A9vXf9zY5VGj/zpryVTyDzbZCNLjmwKh8ZDZG6lkygppmUFZnvZKcbItj92Zi4/xodAe74KLr3Y89ORFFmNz/8x+wyoN97CkOOyXZtMFGSlNS5HZSy7WZUV13XDnT8TGeJo67l+pqnHfWyMtEjZHI/HlVnFARurDf7tYuJDNSgZJ78JqvVUMLBxPYxa8zcZ4peSYo5q4enqj3jkftuH9kUhng3N0Gj7zyMntGUcOpNnsgc6vVuvECA5s3qJ7eMjspeWh9Jlia8b6Zptpbn0n2epMJLIslAiHz0h+aTbrceqgID2/ENyWGutqQBULCJvYAqiyLSF2bWSlAimZZZIV1CZIibRe86kSAuKEOD8tfbsY9aoxYMjCNcW+jXq/ZVtC5NKY5I48OBqJvEyE6BleI0Opj5WQbct6Xcc4YHVIJz2FEn2g8s3PkNi38cwgLf0Ppc+MgHBXfY0esUmS9MbMiA99wkUunCAT1x0D0y2dQWnVLZSB4zLf/HRO2p0ZhBTGD5LbegSoh5xnZ0px51zJn8hfjfHjMDpfktHKLEr3qrRQoptplE8y1xQTm1omVkEK419S0cR1dxlRoGPUOUzxrRYoPSXu2NzZ60yohT3XFqrBRQMjI8QXEHFhQp3e2tqQ9hR0kRCotvdV0yJy3I9npgGaFM7Fq9is8zhlnM91FPOyc3rG9m0YqpEbrF0aUPOWYTu9F+rodLQo+GRG34b8b/cO2r5aBGRJoQVQv4DwF9zCQPYkGWJSZee/5TahRWTZn2nhuY4n7VgFxQKSfORMoWfo5T5FGXECd7SIzPhmMK0D7Z0S9XFi5zzpLCCPOuGRefGQ+cR9FXMt4hef8mf+1aFXBZBbIbHY95ZMb8B7U0NIwfZttndOImOok7hC4eJMnAxmRZ+tIWjv8EErTUN/Zaq9vX1qdZYsHd3d5bwnA9QyqQFFy9oI6fPzep0YNSlu1DptnNv39b623y6IXwdk0YlhuNvU8eAX/1mVCtjOKISU9y6V3derJdeKGF1d6uqaq3jrpCZFtnZuLqt5M+2vnKDZrg6xW7gNEyQ5bBtjdVLjtW95VhK7Q5IGUSBxgV5NvVzxvIWzqaPQIyVRPdiaaB321VVFFGCPjmrBxmoyRpUMm8/jdB/TswAgjOjGBb+GdfQ6j5Ju3wkpFK03a+BE3qwk2Dk6ebNxH90oQPn13bs/IvGqk+41dRLBpyLhldLOE53EUXqNfYkfIQes7u6CnU4pmpJMDkmDOL/R3791v4L4zffwmn8qRCdK7N59SPZqQSIYdqUYdB6+7p6Nnu5qbm76ga0kzFK01KSWykrni6T55KcjLRsVIcU137QVyKRK7F6nceGR2j7cRMgpEJ4V2FjuXHbWk8Tgxx/zU2F1MplJhpKZ3huSiTFpPlVnXv0vZgXSKvnGLFCeS8EO/nG5x9UiI2yTn2Kg/GgkEu90bq5jmy8NyOhVlGQpqWT6+OKK0v1iD2PDxIWJ1sN+2uSVD1EpsjzImhbhnoymKVpS4tMU/55E3C18MYWyCY1USEYr8ZBRXkixa+MmInNcLu2bpCDoh9bXs9MiRlxodbX4sK9vTRJ9hfoCk2LftsIXZ0RKSSsNiqvdW87WN8kz5LfvZCKjYLo2jy1Xi8M5M4/EdIVMevm7c3vhO1sYJcdSiKWKHU/PfXAfgbwTBTbGeYPRI9men+PyzOiMtHNh5Zai9boHo/qlFsKuH/1y1+ankE32/chxqX7PBZvForkjPJTOaGQxTWbdgRTpsw83Hkp+uVTx08h28eyZa5KxLe5HFz6yZ/uGbniu2tBLvpb7dRLfg/W0/aTf7kE4bZiB+q+mIj+ffmznl4TyhwocAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgD83/AEzuxDdeLTm6AAAAAElFTkSuQmCC"
                width={'50px'}
                height={'50px'}
                style={{
                    objectFit: 'cover',
                    objectPosition: 'center',
                    borderRadius: '50%',
                }}
            />
        </div>
    )
}

const BtnRetiro = () => {
    const [modal, setModal] = useState(false)

    const toggle = () => setModal(!modal)

    const styleBtn = {
        backgroundColor: '#000',
        borderRadius: 20,
        width: '100%',
        color: '#fff',
        padding: '10px'
    }

    const styleContainerBtn = {
        padding: '10px',
        justifyContent: 'center',
        display: 'flex'
    }

    return (
        <div style={styleContainerBtn}>
            <button onClick={toggle} style={styleBtn} type="button" className='text-uppercase'>
                Solicitar Retiro
            </button>
            <SolicitarRetiroModal isOpen={modal} toggle={toggle} />
        </div>
    )
}

const LastChat = () => {
    const styleContainer = {
        boxShadow: 'rgba(0, 0, 0, 0.1) 1px 4px 10px 0px',
        margin: '10px',
        paddingTop: '20px',
        borderRadius: '5px 5px 10px 10px',
    }

    const styleBoldTitle = {
        fontWeight: 'bold',
        fontSize: '1.2rem'
    }

    const styleItems = {
        display: 'flex',
    }

    const headerChat = {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    }

    const styleOnline = {
        backgroundColor: '#06f62d',
        borderRadius: '20px',
        width: '35px',
        height: '10px',
        fontSize: 7,
        color: '#000',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: '4px'
    }

    const chats = [
        { id: 1, img: 'https://www.teahub.io/photos/full/88-885387_android-developer-wallpaper-hd.jpg', name: 'Juan Diaz', lastMessage: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore atque molestiae quibusdam enim voluptate quisquam officia', online: true, date: '23 de marzo del 2021' },
        { id: 2, img: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFRgWFRUZGBgZGBgYGBkaHBgYGhgYGBgaGRgYGBgcIS4lHB4rIRgYJjgmKy8xNTU1GiU7QDs0Py40NTEBDAwMEA8QHhISHzQrJSs9NDQ0NDQ0NDQ0NDQ2NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NjQ0NDE0NDQ0NDQ0NDQ0NDQ0NP/AABEIAKgBLAMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAFAAIDBAYBB//EADoQAAIBAwMCBAQEBQMDBQAAAAECEQADBAUSITFBBiJRYRMycYEUQlKRI6GxwdEV4fAkYvEzQ3KCov/EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACYRAAICAgMAAgICAwEAAAAAAAABAhESIQMxQRNRYXEEkYGhwSL/2gAMAwEAAhEDEQA/AFpVoCK0mPFZXEzlA69KKY+ohuhrzuXjk2cikkbXT4gUTQVlNNziOvFGF1RR1NVFpKjeM0GBXHQEEHvVJM9e5q5j31flTNapplqSZmMzw0QSU5HWDWczMm7jNxK16kBWe8V6Wty2Wjlef81Lglsx5OFJXE851HxLkXFKl4HTjvWUvYzM095rT5WEFNE9B0dX8zCRMAVpGaitHNbbN54TuA46eoUA/WKOk0I0XFCrwIjii5FCd7O7jvFWUMvORASx6Vjc/Wt7EL0miXjDTDt3pMfmEmPrFYFr5UxWTTemc3NOV4mwwMozxJrSYxJHSq3hjAUW1YjkiZrQ7B6VHw5bNuKLStlW0KsLXStICrjFxOg6KdTBThWqEdpUqVMQqVKlQBylXaVAxVyu0xjSYHZrhqImpENKwFtpripajehgDss0MvJRm/amh2RaIqTKSAl9ytVDqBPFX8m3NUji1qpRrZjuyC5dmqTjmrz2Iqq681SkvBmGVHLbVBMmtdoukXYBK1Z8PaarMCRW9xbAA6UuSSeiIwyAGPprjtXb+Gy9RWrS2Kc9gMIIrncLNvi0Y9bLnoDVrBvtaae3cVpbWIo7Ur+CjjkUYvwS4mtonsXQygjvXbiBgQeh4qthYht8Ayv9Ku1qtrZsra2eb+LdIa2dwEqTwfT2peE2aCpUxMgxW/zcNLq7XEiljYaIIVQBUY+GHw/+rXQzT+BFXKbtFOq4qlR0JUiHKsB1ZT0IiscvgkMxLtHPEVt6VDjuyJcal2UdKwjZthJnbwD7VepUqaVFpUqQqaadTWpMZynU2nUkMVdmmzXJp2IfSpoNdmiwO0qVKmIVRuKkrlJ7GQCpVWu7a7SSAaxqJnp114qoT3qWxNkzVBdWuG7Ub3RSbJbKWRaFULqRRO6aH36izNg66apOnNW77VTc81rEybFomWqPt7VucR5FecaKk3CD616NgJAFORfCy6tTLUa1ItJHSOFOrgrtUgFSpUqYhUqVKgBUqVKgBUqVKgBUqVKgBU1qdUbtFJjOinVGrU6aQCpUqVIYhXZptdosB00ppk1wtTyESUgaYGpwoQDqVKmM1MBl1JqpdSKuzUbipaE0B796KgR5qxn4u7pVexaK9aTSox3ZIVqpkLRALTLluoobRnMlKrC0aNX8WTTPw1bRaSMsQHloqXFdeDxPvW403IDKCK8lz9ZDt5TxWj8Pa4UUBjxWsuKSVihLGR6StSisuniVB1q7Z8QWj+ascWdKnEOiu0Ot6pbPRqsJmKe9Ox5JlmlUS31Pen7x60WUOpVwMK7TEKlSpUAKlSpUAKlSrhNAHGaKgLyalcU1UqbGICnKKQFdpDFXK6a5QB2uUq7QBw0xlqSuRSoBgFSA1yK4wpoDrNVd2pPNcW3SbJZz4xru8mpltCnfDFMKZVZCaY1ir22mlaQYlD4VcZKtstRsKBNFB7VQG3V+4KqP1oIaPAMa5zWmwL4A61jl+tTLlMvc16so2YuJsMnNHrXcXUAO9Yxspj3rq5TDvU4aFiz0vG1VR+ei+NrI/XXkiZj+tEcbMb1rKXAmG0etJrKx8w/erNjWkP5hXliZRPep0vN2NZPgX2NTZ6uuqJ+ofvUtvUAejfzry3GyXHU0ZwsojvWcuGvSlys9CXL966M8etYe9nvHDUGvahdmQ39amPHJ+lPlPVVzAe9TLfryvG1q+vvRvD8Sn86mm4SRS5V6bwXK7urL2fEKGr1rVkPQ1DtdlKcWGqVDV1BT3rp1BfWlZWSCM0poU+rIPzCoLmsLB2su6OJ6T2mixZxDk0qBHXrYUM7BJ/UQOe49zUSeJ7B6OT/9W/xUvkiu2GcTRUqE42qK8kMIngdOIq0uWPWiM1JWhqSZcrtVBkini+KqwtFmm1GLtd+JTsY/bXAtc+IK7vFGhj6VRfErhuUWIeTTSaYblRNeFKwskY1E5qJsgetMa7QS2NutVN25qW7dqi98TSIbPDkxxS/B0rVyrVt69Z2YWyJNOmpl0qat2blEMVwahyYsmDLejH0q5a0dh2NaDG20VwypNZS5Wg7Mxa0d/wBJqwunOO1bm2ixTGsLNY/MysTGppjnotWU0q4Pymtrj2lFWmZR6VD5mNcdmH/APHM1H+C5o5ruoogPImsouvKW4NaRykrJkqYWTAp5w4qBNYEV1dXBNFSFolXGNWrdth3qquoD0p51NRUtNhotkv604bqojVlp3+qLSxf0Oyd7JNDdSyTaELy7fKOwH6j7VMdVFUbtsuxc9+nsB0FYfyOV8Uf2IDFnYy7Fj6n6kx9OaI4TxTnxYpipFeZKeQGu01CVDQdpMT2n0q7kYrgbknjqB6etA9L1Uqnw+Nu6fcGtJj542n3EGjimoStM0VMHrkv61btZTetBMm+VdlHY8fQ81EMtxXrxWUU16LKjXW75qcXax1vVHFWF1tvShwkWuQ1QvU74tY9vEBB6VNb8RKRzRhL6H8iNK9+KaMqss+u7ugq5jagD1pOLQZ2w816qt164lwGpdoqCuyupNdZ6sbajdaoVFG9eqi93miF1BVN0E1SIaPEbN8VcXIWgyWzUwQ16omkFVygO9WrGogUDS0TUy4rVLiiWkaRNYHrVi3rhXvWX/DPXUxnqMIipGzTxW4HUVXveMCO9AbWnMahytJIqfjgCoNXfHdz8ppg8cZL8QPrQfG0eTyK0OHpSrHFJxgvCrSG41i9knzk0ZteDlAmTNXdNuIkTxRZtXQDisJTldRBY+mfTw43TcavY/h5h3q0msJPUCr9vVEI4IqHKYJRBF/TWUUHzMR6O5+qgUKuZ4arhl2TKvACbTipVD95oiXBroYVtYiilt2PoO5onk27yW2dGZo6cL0gST5eYntSxl3EjpyOfQetXNRzECqihynKsAPnX5TJif2/88E8p8rUkqRcIr0rrhXwyBskODG8bESep8pgyeDx9KH5lx2craICqYZ2G6T6KOP3onp+x3AErHKAKsdpiJ/V2H34qXW8VUYMghXklfRx80T6zP71z8scI6Sb/AEXKNR0BkS4P/d//AAv+asWbuTMLfn22qp+xg/0qLdS0q4fjL9/6GuS3t6/oys02Dis4LFiWEbt0Sf2A/pVz8KKr4D/NUzZJFdn8TllOLT8KVEb4oqvcxIq4jzVu3YBrryaCrM7cwJNN/wBNitT+CU1MmlqetP5R4My2Pi7aIIyqKMXNHXtVa5pfpSckwxkiKzloParQzVHeqFzSm7VWu6U8daVRfoXJBf8A1JfWo31FfWstkYF5T3qrcS8OtaLii/QzZqbuor61TfPE9ayl67dFD31C5PStVwonJgVcRh1Q/salXEY/kJ+1HLGpOiPcKSsSI5kVVx9eBPFtpboBz9uK6Ll9E7BfwWUwVIPvR3C0p2XdsoZczQ98K5Kk/wDaePrxWzx89LG1XHDDhuxPp9amcmkqADpp7foPFV7toKeVitVkavYCcfMenUT96E4rLkyoTkdW44rOM5dtACLN/mi2PiF+YrtjTQHKz0o1jqV8o4PoRFKU/oCgmnR2rjWSDwKMMjnrAotp2KqiTBNYy5GlY4xydGSW1J5EVL+GB4ijmq4Ydxt49ahfTXEBef5Gkp2rG406MzkYJnip8fCYDqaJkKH2Fhv7r3H2pq3Rv2BWnvx0rTJtE0CsnBMTVe3pzx0rVpjz2FduYcc0ly1oMTOWNOLGOkUT/wBGO3jrV21Ygz2opZb2qJcjKUUZG8nwdwcddsSODwZg+3FAbmS25vKGBaC3k4HXczF59CTtn60f8W5fnKqJChQ3TgnkTPTqfX94rJ5+YyBRs3SdsrtlenB4689/71cdqzSKov2c1EZQoJky4/Uo7kSIPuY6D6Ud1fK3Y6Ox5VkPXqH8okevPP0rHpdaWChQX5J67gJ68wo+h/vRbVHLYbwRC7AAJ42uCR0ABBVuPr61lyQyVFPolmqGn55/GW7a+rbz6eRiFHv0NVv9WBQBJZ2AAABIBMDk9O46kfUVY0HFW3dQt1kyx58zA7jP1PWuBceEW5L7pf8ATBKuzcYb/P8ASmW7m6kilCyxuJHG3kfWuY6N0Cmr/gqk7AtK8VYTLiqOTbcDlTFV0vtPIP7V21Y7oP28+imNlKR1rP47qaspeUd6hxLjJoPteFQveFDVviOtJiD3qaLyLxvikbooWye9JbLfqooWTLGQ61QcKahyrL9jVG4rrWsY/kiTJMqysUDuYyzUmVlt6UJuZbz0rohFohsymLqGT0USsfKwER6Cao42XsfeqsCDIAJ4Pf8AvxUsMSJcW59XAP3A6fekue9rcqXA0kgxyCIiQx6/7mut/g0S/BvNO8bY62x8VN3Tjyu0+se1QZXitLsA49sozAS7oIM8MFB3f3rGaZZN9trA7QpMqqiCBAggf56UeYraVFa2HR4tygAR+oPK8h4M8+kietZfHFPrf7JaS0anAzkW0x2WnCtINt1ubR9/MP2qhp2qIru9lIR5L+ZVAPYjmIPNFtL8P4AIK21WJAYu8kE9TuPShfiDE0y0WVLSvdBHlUttVjEbyX6cgx3rJNNtU9k4rw1OFlYqbCX2ufynzgH3I7VJk5tjMMWryoyOUdgCeQOgmJHvXmun6pYEi6ArFtqi1t2xBkv5gqiYiOeD1ov+Jwd63rmTctKVKlbW7aLiEQykK3BHJ95mlLiV3sqn0afUsG78JkR0e4ZKMG2btvJH1ih3hfWxi2Gu5Tu25tgAhtrDiCR61l31dXfz3brqjO+2EYhJEOBsTbIPyz+1WtM8XpYQqmBbe3vJe4+9pkiCytu2cEcSRJ4oweNPYKLsM5XjtLrlURlE8FvKTHpVrT/G2Q10WUxt5IAUkweehYenvWS1jxuL9+y1nFRFt7lCHzb3fgN5QCI7AeprW6VoGouLl2+6Wi6kLwpuIvopM7F9pmhxjFbSX+R4tMr6joOoXMg3nQB0HJQgz6BVBoSiakuQNilQ3BLwDHckdR+1WsLIW0XsG7n3dzbSySsvJkByvy8DzBvzcVB+PyL+RtsNvtAbPjXTbR0K8sou9CRI7GiN9Oq/QqLOt6j8MqiZDLc3ANuHl3HsOJNafRcgojXMhjtBCB3AVWY+lAbeCtu/aOe9rIQ72V2vKwQKAdxtqFDAffqK7e1XAe98N80fhnBHwLaMiq5IIJuFduwiekGY96UlapL/ACCiakX0d5RkZFjftIIE9Oal1rWBjKh2Eq3f8o+p6A1mW0vAs3Qtj8UV2ki3bCvbZtsyzkdY5EtHBora+NnWHt27T20RWQrcPFxiqsgZmEgc8xMVk4q0/PyPfhldQyviO7qRBck9COvQntEfsPpIXMKNz5pAHMvMLJ3SOvB6mt/a8BXDilXe1ZdAzIbQZkB28/ELCWB7wO8joK8w1BsiyNzWyUMbbiA/DInyw0ESeoBg9DHIrRU+mWk/R9u6Une3kPQdAnIAltvcwCe4FTZ+Sq4zAHl2CgTJj5yfpx/MetAsrMY7R8NgVIiQR0BA4+//ADuS07Ae9Ny4VhFkpJDKPykrHQnnr2M9gXpbfmxtpIOYOKLaLA/KOe5onjYd54ZLDkSBuKkLz7kc/aaGaUSbgBJ5ZR+7CtJ4y1nJtK2zJVlLC0uxNpDtIUFm4jgyR0ivO4+H5ZvJmCVmr0rCZdqMSAo7iNx6mJ7SaKW8RF80jnvWU8MnIWwbWRdGSTDKynylWI8puNJZfQ7elGbWtWQj27ifDCD5WhwUj5gBJjqOfSuh8daRqqQTvX7Sgyy/yrPaxr2LYQu3m9FUAk/SgZ1XTsxLiKhC2z5bk/DDsBIAhg5Ht/tWbu3bVtEbJIZ4P8MFVVQASAzEF2aOd3StocSb3f6Jk2ejY19LtpbqLtVhu5gED39Kr5OOF8wJj/nesNl+JbVzHZBvt2j23CS0cKCq7m+xAj6UU03xDmXMf/psVHCRvcsCjKOCiKDJaPv7UPia2LsL2MwOSFbp6mpL+ohFncrR2DCfeg2R4tx7VwF0dCEMfwx85PQTHaY+lV7vjTHN8IMdkDwN7oFIZhwWX0kj96fx/gSToI5fi1EQFVBMgHnpNXx4hQKpJ615rm6YjZB/6hVZzKhbblTxz34PB4rQpi4iBd+TcYiAPLt3MBJUJE9qt8UPyFvxmouawvWCfoCaq3NctEhd3J9RFZvUNbBtn4F17TAkruRWNyIhBz1Mjjk0Dt61eR9uS68rMbZPm+gj7+9OPCqHtmxyc+1ugus+k0KytQsq0FhMVjsl7bOTucj1SCAOeTP2qneuqDCrIHEyefet1xpBjZ1/LI2jn/tG6fr1H2qU2lBCm0wbvBDNETwoHH3qBct5EmYPU8n+dXm1EAEooWRBaOfaZqy3aLWm5CWlbch28Tv3Ce4AVf5zweKkTV2LAbEVeFCrCAqDM7WO1enLdoJniaAvkFmlvN046dPpRTHy3uKwK2CoHRwq7QfRzz/Ok16Jx9Zoc3VVZAPxI5UlUVWHnkRb+LDAr15gdunfP3fEF0k7raGDMbWCgg9SFYBuhEn+1DluJJDJ5fygE8H1LTyKvYToDtuK7gxtSdqEDpuYHd+0fekopdBSRft6LfvBLrIqI4LL5gDtCzuVSfKGjq3PM9OaVzBZVMX8YBts7XZVUdh5VgGesMSf3NX8MY2UGbMyBbZTttqjAC2v6YZOR9/60Ey7VlV3AM0sQgeQrICYYbWG0n3nrUq+mFhvFtuiEXvwxsT+banxJAH8J0BZpjdO0r9I4gy9O/EW4sOhFsQUALOwB4G6Bvhek+hFRti3LqJuTbbtjcUXzsYUbdx3bj5ePbpApY+ofBcsMd0SFEhdrHaPzAiG3H1/rSS/sL+gz4S0q3j3g9+5bsBLi7GuW1e7dYKSdksUtKJENzP2423inxljW1KDIvByFICIu1g0gjewCgQJkGfSelebDxs6vvOPbdtxIFwu0AwOVBAZjHUg8cCOZku+MvxGVbe9h2mtr5TaRA0mCCxBBDEAmAQYA49azlxNu2Um62QprqO7oGuXgdwti84RGlfz7CCT1Ak8iB5aLaFaxiFe4gR18xspuO52EAGyoECOgZ5MT0M0dfGwHDPifhI2NuQ223tMSA+4BeRxAEc81UVLTgpju2E5lm2OlxH6gbmfzhzwJZgBB4pN2umiW0U9VzryztsImOxHlFh/ibgAfM7BhHUgjdA4461l31dC7utt3c9zsJ4/VtSSOnfsPSjWBot29fuWM3NuIiJu3B96mW/MGaOk8D144FbDTNB0tLduwpF0sTL7lDMf1sR8omAB7UOSjqg0Y3TPFJCA3bD3HRmYQ9xFG6QqqitAABiI7czVqx4/vAbbNkWp3MAu4ru4O95bzTCjnp070e8YeFrSWHeyiW7g2KpDOwuBgBsB6K8sYPIjr2jP6bi2bKuMhbyXupS3bDNu5KsHKBSDIIUREQSeaI4yV0D0DM/xtnOGS67MzqQqjyqskrKqsSeokyOPrQRdTvguqO6I/wA6K7FGMc7w07z1mZr0HwDg4dx2fIuo+QYZUYg7QZIlm4c8yVEgGPSn6tp+J+KOPjbLdwQ38QF7Z3AE7dssGZiBPaD6mi6bSQ7R5rk33gMykTAkKFBjjgAQOlEMTMvBCAsBvmMDcyxwrE9p2n2+5r2fRvD6WcZLLmyqkxcVPldnJEbmEmTA5+lV9S8L21Q7LQdpCLsMCGMS/wCkLM8SOOgNZqcLega10eNtqFwPtQlWntDdOSPc1rdO1Zmx3RbNoO683biKzNJAZm3HgBDx2EUWx9Awk3Bt6OQztfAueQgkQFMqEgxzJJ6xxFTM06xaQOXbKLl1RCXUPJ80oilmjjnd3rSOHi/0S2vCbwj4ZzyQbt4pYEm2hVXDwIG0N/6a8g9AT5oHIJn8V42ZiqWt2jkWwJ+IGYbPXfZUyyj9XSJnpQjUfFWpWX3Xv4C9FG1jCzA3LJPMdZE1f0vxxl5LbMfHDkfM7kKigjvBhe/BLGk1O8tNFafhj7uEHRGv3rIdizbeVcBiC29VUDf2AJ6EfYto2sYoVLN9LVxU8tvfaAZG3yylyWDIT3niR16Vf8VaPtuTtcEoGYOoe1dYS0Kyc2+WHAEce5NAdn4YG3sxmdwLm5SxewGEDbdMR6wp6+taakhZGl3Y+Qz2lNnaQr73CEFpcIEYIwlQxUn6cREPw9QdLT2bl7FHwx5FtuYuFB5A2weVNxHWOVPSaHWfEONaV7d1ruQWBV3adrJyQpUNDAEnkjvQ/I8TYvRLCADp/CQT9wwI+1JcbvrQrbXRdsg5Ns3Uf4l1X8iyLNvywWNuTubqYYgGVHAqve1LNSNz3ChLLFsLtkGCPMCzCQRLbQYqodVW4hVkT4R/LvPBmdyqflM+h700atd+EiIF8sLuZoEKCBwTyxnr6zVKDXgrLxz7l1wrWFJAbYjsoQKzElmRVKhhA5I9IoLqeW8je7K6rtCWiFUQYgKB5eO/tWmxNYW8+1WKMoHJAZEgdSV46HipdTa2b6FkR22kK5PG2OeO/wB/Wmm06oSlXhjbeoLxvd2MCd7OSImQpBAj2NQ5eqs4KlQw7FpJWPTny9eg459Kt6stp7zQ4RQo+VfmPccVWsNirPzsexbgfsK0otV2VbKdCVJJ7FSFH1qO/wAGPL+w/wAUR1DVy4CBQB3juPahDsJ71S6KV+nD1qTZ5fmEdYpUqSGM2GJgx6xThzwqz9Zn/ApUqEIurhqylndUjgKFLH+Ro/pmrYllF2ozuvLMV+Zo6kn5R6VylSkie1sF5eoWHbc6F27kQgn3jrXdLxkH8QW3dgwKrClSP6zXKVDVJh0jQ4OkZuW/xERrQHHLQoH/AMY5P1rRY/hDJdHXIyXgwPKF4A+1KlXJy8skJIqZ3gB7dsNaLXXVDyQAx7hQO39aC+GMZ7G+5cHw3DRBgNER0P179qVKnw8kpppil0DdTy4v7raMViY2xJXifKPN/vVC7evS125bu+Y9dpVIHQSRyKVKunpqikgza1/Gt2Hspju5uCbzswMmOqAfLHMHtNMwNXxrB3LaJB+XdDELMkKSJHPp6UqVLFWDijVZNk3LAe1d2qp37rm5jJA4CuTBEDpQzwr4UfOZ2OQERGCFhLu5Ik8Ewo+s0qVc7m4wdEw7NRf8AYOLj3IuXfishAcPDz1AVVABExx3igHh3Bx7Twy3y+0B7jgL3JOxWEqCTx34pUqmEpNO2OfZpQlhE2B7gQiPh7hBn1BEn96safnWsRDbtoQCS53OzSx92J9uPalSoUUyG2mBtS1jJyGItoyJBDbTG4Hr1oPjaq+PeFzYdloFERYBJaJLGlSraMVuIehjLzcfLTc+OFeDLkLPP16j61YwLSXbc37xZDC7EhJI482yP2pUqiUUlr7DJg7L1H4ZW1h2TcIchl5YKvaSenag97RsvKuu+Ra2Ku3hYAYDoBHWK7SpZOOkNdFUeFUtneSzqssyEckdgY7Vn8rADtvtBVBklJ4Uz056UqVbQbd2Ck7FYwktMrZADrPyKep9yO1Wcd8f8TvuKqJ1VF+Wewb2pUq0rRYQ1rxHtttZtKqg8EqI4PpWXx8106MSPQ8ilSp1SHFKhXL4ImBPeKrAUqVMYiKVKlSGf//Z', name: 'Sakura Sanchez', lastMessage: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore atque molestiae qu Lorem, ipsum dolor sit amet ibusdam enim voluptate quisquam officia', online: true, date: '23 de marzo del 2002' },
        { id: 3, img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTbg-BHRaLR6P_l8nJFwlcrzwF5m2QfMwoZgQ&usqp=CAU', name: 'Naruto Uzumaki', lastMessage: 'Lorem, ipsum dolor sit amet Lorem, ipsum dolor sit amet Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore atque molestiae quibusdam enim voluptate quisquam officia', online: false, date: '23 de marzo del 2022' },,
        { id: 4, img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTiYms7u3GRL66dqgnCZip4snsCiw31DnMvuQ&usqp=CAU', name: 'Sasuke Uchija', lastMessage: 'Lorem, ipsum dolor sit amet Lorem, ipsum dolor sit amet Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore atque molestiae quibusdam enim voluptate quisquam officia', online: false, date: '23 de marzo del 2022' },
    ]

    return (
        <div style={styleContainer}>
            <div className='text-center' style={styleBoldTitle}>Ultimos chats activos</div>

            {
                chats.map((chat, ind) => (
                    <div style={{ display: 'flex', flexDirection: 'column' }} key={chat.id}>
                        <section style={styleItems}>
                            <div className="w-100 px-2 py-2" style={{ display: 'flex', flexDirection: 'row' }}>
                                <div className="col-3">
                                    <AvatarChat img={chat.img} />
                                </div>
                                <div className="col-9">
                                    <div style={headerChat}>
                                        <div style={{ fontSize: 7 }} className='text-bold'>{chat.name}</div>
                                        <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <div style={styleOnline}>Activo</div>
                                            <div style={{ fontSize: 7, color: '#9e9e9e' }}> {chat.date} </div>
                                        </div>
                                    </div>
                                    <div style={{ fontSize: 10, lineHeight: '10px', paddingTop: 4 }}>
                                        {chat.lastMessage}
                                    </div>
                                </div>
                            </div>
                        </section>
                        {
                            (ind != chats.length - 1) ? <hr style={{ borderTop: '1px dashed #CCC' }} /> : <br />
                        }
                    </div>
                ))
            }
        </div>
    )
}

const AvatarChat = ({ img }) => {
    const styleAvatar = {
        borderRadius: '50%',
        width: '50px',
        height: '50px',
        objectFit: 'cover',
        objectPosition: 'center'
    }

    return (
        <div className='text-center'>
            <img src={img} style={styleAvatar} />
        </div>
    )
}
