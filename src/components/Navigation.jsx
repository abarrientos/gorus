import { Navbar, Nav, NavDropdown } from "react-bootstrap"
import { useNavigate as useHistory } from 'react-router-dom';
import useAuth from "../auth/useAuth"
import routes from "../helpers/routes"
import "../styles/sidebar.scss"
import "../styles/variables.scss"
import { useEffect } from "react"

const linksNavs = [
    { id: 1, path: routes.home, icon: 'home', title: 'Inicio' },
    { id: 2, path: routes.projects, icon: 'home', title: 'Mis productos' },
    { id: 3, path: routes.home, icon: 'home', title: 'Lobby' },
    { id: 4, path: routes.home, icon: 'home', title: 'Wallet' },
    { id: 5, path: routes.home, icon: 'home', title: 'Cerrar Sesion' },
]

export default function Navigation() {
    const { logout, isLogged } = useAuth()
    const history = useHistory()

    const closeNav = () => {
        document.getElementById("mySidenav").style.width = "0px"
        document.getElementById("main").style.marginLeft = "0px"
    }

    const openNav = () => {
        document.getElementById("mySidenav").style.width = "250px"
        // document.getElementById("main").style.marginLeft = "250px"
    }

    useEffect(() => {
        if (isLogged()) {
            openNav()
        }
    },[isLogged()])

    const redirectLink = (link) => {
        if (link.id === 5) {
            logout()
        } else {
            history(link.path)
        }
    }

    return (
        <div>
            {
                isLogged() &&
                <div>
                    <div id="mySidenav" className="sidenav">
                        <div className="h-100 w-100 ms-4" style={{color: '#FFF'}}>
                            <div className="row align-items-end h-100 w-100">
                                <div className="col row p-3 w-100">
                                    <Avatar />
                                    <div className="text-bold text-center mb-4" style={{ fontSize: 24 }}> Usuario Play </div>
                                    <div className="mb-3">
                                    {
                                        linksNavs.map(link => (
                                            <button key={link.id} onClick={() => redirectLink(link)} className="w-100 mb-2 link-nav"> { link.title } </button>
                                        ))
                                    }
                                    </div>
                                    <button type="button" className="mb-3 button-primary text-bold">Nuevo producto</button>
                                    <button type="button" className="mb-3 button-primary text-bold">Valor de membresia</button>
                                    <button type="button" className="mb-5 button-primary text-bold">Iniciar live</button>
                                    <div className="text-center w-100">Play v0.0</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </div>
    )
}

const Avatar = () => {
    const contentStyle = {
        alignContent: 'center',
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
    }
    return (
        <div className="mb-5" style={contentStyle}>
            <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQoAAAC9CAMAAAB8rcOCAAAB71BMVEUhvY7raVJusaCzs7NPptv6rRfim5cAAAAhwJB+Rt0iwpIixZRwtKO2trYewZDmnppzuaf2blaokXL/sBSCSOTuo59SquKgmJsdw44auIlloJFVhXkANiT/tRgdpn1KcWgSh2QLAAAAWUAAkWsUgWEAmnAAIxUArIEACAAaely6enmEQ+Y9fWqnoqRqqZkAlXQ2TklLmM/4aVIwAAAHZkpDZl4qAAB2eXgAQTFMem5qcmKNb2h5cWSEezadeyU5bFXbi4tpXVSJaR3/qQB2ZylefXKYj5OFfYBFUUQpdl9iVyRvb29FUIZJTYs4aVsAkHLomgAPWUcmWl2paGkIQDlQflAdGAlaLZxoKbMJg1dbRUJ1PcwARB5TcF3KgQCeZxipRUC9T0aGLCVTP5YnbmlORkk9MDVYPomIROwPq3UAFgCdoJdlNrGATk+jUDRMKoVcLzOJWxEhADI1ZHZCRD8/WUPOWEVSD4puJrwHl2NDAG9sPL5TZFweAAAAKAAAZT0AOxAwAE+YZ2RTMQAAGx4bPUAtAEoGcEqzdwA8b0qTTz19PG+KQGNaMZdtPEC9dQBGFQAdHB+AUj8vdYhwJBhwXEY1cJMqc35kZURhKyEfN1MtU3lAGxUSFy7RTDwkRWFqf0drPQAXPUVBfrAWIji56NPHAAAMRUlEQVR4nO2bj1cbVRbHk1CZx7zJZNIQyCSQTDITpuFXgYIkdhVcWmisIhR2KdZW6C+birZaTVu1dkW7xe7WQi20WmV/4B+679dMJtBq3WO147mfc/QcCcTc79z7ffe+9xIIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMCzCFIJj39VlmX0G36a3xNsvrD/T9ZjtbBefOlFQ37C9yKa2ratqv7UDo+MxiORyMsqzY4dIdt/HovFYmMHnuitkF44ODHeOj5x0CJioETi8bn2LIKM0UgjIX4Im92lsl7/qnyg/TlK+ytP8pz1w2FBw3A+Yb76mpV4Kp/5KaHuZ0o0RjpzSUVRJuvyAr0+9hxnavDna0Qthxscwq3TXc3NXUfwU/vgvz7qDJeisXFWC4VCvQXv45f/EhNStP/154NSSzUpGsJzzU1NzUfnfeQa6rKQIv6GQqRQOrwhyy9xKci/923/u8QOK0BWQ7iB1wfhaBPh2JtParjPAOrxuMiKxiGSFsk2rxT6CSZF7K2FxZPpuqAS1mtzp04n6p+5OtTa0Hr4zNkJokX47S6SFXN+kiJgzwgtIp0pTUnlPJ9dWEXsrWAwGF2wPXEnXiVOsHRurrJNi8Hz53NW2hwn2fHOHPkNKe+jAgmg9LujcbKcci2GLM9nF1YRey9ItSjWREoc6aLZf+Hc7e0rDsYyQribpsU7e6Tzpp+SgnYDOSl7caYx0hgZzdxIk+bSeUVYhZBCqpVOhRlBU9OFY2+6P0TEPURrhUxmnGfS2E85wSDPUje/JP4ZibxbMAuWLeITVtH+PiuQmougq11ciualDyrCL9TKpVOnLlW4k+rjNC0mLD8tpS4okEp+SJqtSHy0c6azzGJwrOKE9FG1elJKu4/YlaKpuevokUqCNNrYunyM9BFXDPaX6jBbRIbL/mo3OSifVLShUWafxDdGaETcKmJT0l5sTOe8rlm5woX4ZG7p2Lkrpy4ZV+c//qSZ/uQKayREsxVumPaXVzCQQboK7VPRY0RmTORYRexaAdPxlCmB+KSVuMRt89jtz67/7fOj5851dTWLNPmcGilKt3ItJgzfuUVALdAGK+T0W43UGJhVtH9RrFW8bHV3sxk28TZZTD/pWpLSGKvzbZebmp2KmeNpkeFStBb9lxZqN5VC+9BJC7JcMKtof/9LEQxJDFwOKUqIFg9K3Li8NPd3KU9fRLhy/baTFXM32e+TyYxpIfnPOdUSk+KNzgjvtkj/Ta2i/S1J5ymOX39xZeVAivxWL0t6nL950wiI9EeJVy8c41Jc4I0J0vuoFuGz9u8V0f+JjHUaZKhX4lLELxZUahXtK5JwS/mrqRhhqqSFtFn23JHq2dxCZteFpuZmkhrOHyCb2kX4ZdNfZoELtxbuprQQabvNzngk3vj1HhKAPdV+wllBkTHGu62plKI9KulJ/7n0j3/ebvogJ15EZg/zzScY7p8VEOmTDw1Eg9HNIU3JFLB59uuLUpG+ot9Z4V4QqI2oz8UOKL0dj6r/xHQ1Go2uuiOMOsEKpLXNL2aBcNpMGwO0sQ5udmt9JhEmndd5h7V3lkyjZPkMYGSfcKRYERMb6bPrdjDx3Sh5j2rBMdl8D19N/SIFsu9uDgRPRpkU0VVtjZocciJEMpKx+WBx865dk+KOkqHGKJvfbJy+6tmxwAvsXZyMQQZLih6p4BOv0NnnXwhyKe4l610AYbv8XpCkTHRRX3GkeEnrI4M3MvpbWlr6NwoVJzXUdfqL1ZoUfC3N+SQp1JGBmhIkK1KebCYpYaxvDvCEGShbQoqxkrZG+snERssuQkvL1v15XijIXohGq1lnxucFEj5j+yQp8C1W30KKoFTbt5Fx+vii0IFK8QC/QtfS2NgDRaGpo27tEpDU6L5Kp1Nktz0vFVwt+XB62PKVFDQhCMF9mYxoj7Bu3docCNYYIG740crU1EpJUdiWn8gKR41vLF0lhotqWx18OA235nyylsrHWbzVhW9X792T+pgLBGTVXK/WEoIpcYvUBDZKSdJ2KynaKaD8lkcLWiin8/W7vurBsK8ab7tKQ45WpcneZK+mZWnEhYWgNyFIxix+Syse5XvZlvgQ67vl+e+2WurUIB7q1ULt5lL8XqH9UuQ88broolRIKTRM8gjxSH1CRKsn2wzuixabXbU+3oCiwHzbw/761Dji0QKXuBT6Y/7XzxyyXXxe6jDtJAlTSRZlZFa9OgQXvh+0xQalWuZSZN22A9vXf9zY5VGj/zpryVTyDzbZCNLjmwKh8ZDZG6lkygppmUFZnvZKcbItj92Zi4/xodAe74KLr3Y89ORFFmNz/8x+wyoN97CkOOyXZtMFGSlNS5HZSy7WZUV13XDnT8TGeJo67l+pqnHfWyMtEjZHI/HlVnFARurDf7tYuJDNSgZJ78JqvVUMLBxPYxa8zcZ4peSYo5q4enqj3jkftuH9kUhng3N0Gj7zyMntGUcOpNnsgc6vVuvECA5s3qJ7eMjspeWh9Jlia8b6Zptpbn0n2epMJLIslAiHz0h+aTbrceqgID2/ENyWGutqQBULCJvYAqiyLSF2bWSlAimZZZIV1CZIibRe86kSAuKEOD8tfbsY9aoxYMjCNcW+jXq/ZVtC5NKY5I48OBqJvEyE6BleI0Opj5WQbct6Xcc4YHVIJz2FEn2g8s3PkNi38cwgLf0Ppc+MgHBXfY0esUmS9MbMiA99wkUunCAT1x0D0y2dQWnVLZSB4zLf/HRO2p0ZhBTGD5LbegSoh5xnZ0px51zJn8hfjfHjMDpfktHKLEr3qrRQoptplE8y1xQTm1omVkEK419S0cR1dxlRoGPUOUzxrRYoPSXu2NzZ60yohT3XFqrBRQMjI8QXEHFhQp3e2tqQ9hR0kRCotvdV0yJy3I9npgGaFM7Fq9is8zhlnM91FPOyc3rG9m0YqpEbrF0aUPOWYTu9F+rodLQo+GRG34b8b/cO2r5aBGRJoQVQv4DwF9zCQPYkGWJSZee/5TahRWTZn2nhuY4n7VgFxQKSfORMoWfo5T5FGXECd7SIzPhmMK0D7Z0S9XFi5zzpLCCPOuGRefGQ+cR9FXMt4hef8mf+1aFXBZBbIbHY95ZMb8B7U0NIwfZttndOImOok7hC4eJMnAxmRZ+tIWjv8EErTUN/Zaq9vX1qdZYsHd3d5bwnA9QyqQFFy9oI6fPzep0YNSlu1DptnNv39b623y6IXwdk0YlhuNvU8eAX/1mVCtjOKISU9y6V3derJdeKGF1d6uqaq3jrpCZFtnZuLqt5M+2vnKDZrg6xW7gNEyQ5bBtjdVLjtW95VhK7Q5IGUSBxgV5NvVzxvIWzqaPQIyVRPdiaaB321VVFFGCPjmrBxmoyRpUMm8/jdB/TswAgjOjGBb+GdfQ6j5Ju3wkpFK03a+BE3qwk2Dk6ebNxH90oQPn13bs/IvGqk+41dRLBpyLhldLOE53EUXqNfYkfIQes7u6CnU4pmpJMDkmDOL/R3791v4L4zffwmn8qRCdK7N59SPZqQSIYdqUYdB6+7p6Nnu5qbm76ga0kzFK01KSWykrni6T55KcjLRsVIcU137QVyKRK7F6nceGR2j7cRMgpEJ4V2FjuXHbWk8Tgxx/zU2F1MplJhpKZ3huSiTFpPlVnXv0vZgXSKvnGLFCeS8EO/nG5x9UiI2yTn2Kg/GgkEu90bq5jmy8NyOhVlGQpqWT6+OKK0v1iD2PDxIWJ1sN+2uSVD1EpsjzImhbhnoymKVpS4tMU/55E3C18MYWyCY1USEYr8ZBRXkixa+MmInNcLu2bpCDoh9bXs9MiRlxodbX4sK9vTRJ9hfoCk2LftsIXZ0RKSSsNiqvdW87WN8kz5LfvZCKjYLo2jy1Xi8M5M4/EdIVMevm7c3vhO1sYJcdSiKWKHU/PfXAfgbwTBTbGeYPRI9men+PyzOiMtHNh5Zai9boHo/qlFsKuH/1y1+ankE32/chxqX7PBZvForkjPJTOaGQxTWbdgRTpsw83Hkp+uVTx08h28eyZa5KxLe5HFz6yZ/uGbniu2tBLvpb7dRLfg/W0/aTf7kE4bZiB+q+mIj+ffmznl4TyhwocAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgD83/AEzuxDdeLTm6AAAAAElFTkSuQmCC"
                width={'120px'}
                height={'120px'}
                style={{
                    objectFit: 'cover',
                    objectPosition: 'center',
                    borderRadius: '50%',
                }}
            />
        </div>
    )
}
