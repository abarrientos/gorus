import React from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import routes from '../helpers/routes';

const HomePage = () => {
  return (
    <Container>
      <Row className="mt-5">
        <Col xs={{ span: 12 }} md={{ span: 6 }} className="mb-5">
          <h2>Bienvenido a Gestor de Tareas</h2>
          <p>!Aqui podras gestionar tus proyectos!</p>
          <p>
            Marca tus tareas como terminadas, Agrega, Elimina y Modifica tus tareas.
          </p>
          <div>
            <Link to={routes.login} > Ingresa </Link> o 
            <Button className="ml-1" as={Link} to={routes.register}>crea una cuenta</Button>
          </div>
        </Col>
        <Col>
          <img
            src="/img/task-manager.svg"
            alt="home-page"
            className="img-fluid"
          />
          <p>
            !Gestiona tu tiempo, mejora tu productividad!
          </p>
        </Col>
      </Row>
    </Container>
  )
};

export default HomePage;
