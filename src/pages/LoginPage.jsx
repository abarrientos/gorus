import React from 'react';
import { Container, Row, Col, Button, Form, Image } from 'react-bootstrap'
import { Link, useLocation } from 'react-router-dom';
import { useState } from 'react'
import { api } from '../helpers/axios'
import circle from '../assets/circle.png'
import bg from '../assets/bg-login.jpg'
import useAuth from '../auth/useAuth'

const LoginPage = () => {
    const { login } = useAuth();
    const location = useLocation()
    const [ email, setEmail ] = useState(null);
    const [ password, setPassword ] = useState(null);
    const [open, setOpen] = useState(false);

    const logIn = async () => {
    const body = { email, password }
    console.log('body :>> ', body);
    await api.post('login', body).then(res => {
        if (res) {
            login(res, location.state?.from)
        }
    })
}

const containerStyle = {
  position : "relative",
  width: "100vw",
  height: "100vh",
  backgroundImage:
    `linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url(${bg})`,
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover"/* "98vw 100%" */,
  backgroundPosition: "center center"
};

  return (
    <Container style={containerStyle} fluid>
      <Row fluid lg={12} className="pt-5 align-items-center" style={{height: '100%'}}>
        <Col lg={5}>
          <Row className="justify-content-center">
            <Image src={circle} roundedCircle fluid style={{width: '300px'}}/>
          </Row>
        </Col>
        <Col lg={7}>
          <Row className="justify-content-center">
            <div style={{width: '70%'}}>
              <p className="h1 text-white">Login</p>
              <Form>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label className="text-white mx-4"><b>Ingresa nombre de usuario</b></Form.Label>
                  <Form.Control style={{borderRadius: '20px'}} type="email" onChange={(evt) => setEmail(evt.target.value)}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label className="text-white mx-4"><b>Ingresa tu contraseña</b></Form.Label>
                  <Form.Control style={{borderRadius: '20px'}} type="password" onChange={(evt) => setPassword(evt.target.value)}/>
                </Form.Group>
                <p className="text-white">¿Olvidaste tu contraseña? <b>Presiona aqui</b></p>
                <Button variant="dark" style={{borderRadius: '20px', width: '40%'}} onClick={logIn}>
                  INGRESAR
                </Button>
                <p className="text-white">¿Quieres generar contenido?<Button style={{textDecoration: 'none'}} variant="link" as={Link}  to="/register_play"><b>Registrate como usuario Play</b></Button></p>
                {/* <Button as={Link} to={routes.register}>crea una cuenta</Button> */}
                <p className="text-white">¿Prefieres disfrutar del contenido?<Button variant="link" style={{textDecoration: 'none'}} as={Link} to="/register"><b>Registrate como usuario</b></Button></p>
              </Form>
            </div>
          </Row>
        </Col>
      </Row>
    </Container>
  )
};

export default LoginPage;



/* import React from 'react';
import { useLocation } from 'react-router-dom';
import useAuth from '../auth/useAuth';

const userCredentials = {

}

const LoginPage = () => {
    const location = useLocation()
    // console.log(location, 'location')
    const { login } = useAuth();
    return (
        <div>
            <h1>LoginPage</h1>
            <button onClick={() => login(userCredentials, location.state?.from)}>Iniciar sesion</button>
        </div>
    );
}

export default LoginPage;
 */