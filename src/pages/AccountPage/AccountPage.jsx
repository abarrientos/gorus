import React from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap'
import useAuth from '../../auth/useAuth';

const AccountPage = () => {
    const { user } = useAuth()
    return (
        <Container>
            <Row className="mt-4">
                <Col xs={12} className="text-center">
                    <img
                        src="/img/male_avatar.svg"
                        alt="profile"
                        style={{
                            width: '200px',
                            height: '200px',
                            borderRadius: '50%',
                            objectFit: 'cover'
                        }}
                    />
                </Col>
                <Col className="mt-4 text-center" xs={12}>
                    <Card style={{ maxWidth: '360px', borderRadius: '19px' }} className="mx-auto p-4">
                        <p className="text-center mt-3"><b>Nombre:</b> {user.name}</p>
                        <p className="text-center"><b>Email:</b> {user.email}</p>
                        <p className="text-center"><b>Rol:</b> {user.rol}</p>
                        <Button variant="warning">
                            Editar Cuenta
                        </Button>
                        <Button variant="link" className="mt-1">
                            Cambiar contrasena
                        </Button>
                        <Button variant="link" className="mt-3 text-danger">
                            Eliminar cuenta
                        </Button>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

export default AccountPage;
