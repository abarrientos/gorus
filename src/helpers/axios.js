import axios from 'axios'
import env from '../env'
import { storeData, getData } from './AsyncStorageF'

const api = axios.create({
    baseURL: env.apiUrl// url base cargada de archivo env.js
})

api.interceptors.response.use(function (response) {
    if (response.config.method === 'post') {
        if (response.config.url === 'login') {
            storeData('GORUS_SESSION_INFO', response.data.VS_SESSION_INFO)
        } else if (response.status === 201) {
            if (response.data.token === undefined) { // Si no es login
                alert('Registro exitoso')
            } else { // Es Login
                storeData('GORUS_SESSION_INFO', response.data)
            }
        }
    }
    return response.data
}, function (error) {
    console.log(error, 'error axios')
    alert('Error en la API')
})

api.interceptors.request.use(async function (config) {
    const token = await getData('GORUS_SESSION_INFO')
    if (token) {
        config.headers.Authorization = `Bearer ${token.token}`
    }
    return config
}, function (error) {
    return Promise.reject(error)
})

export { api }
