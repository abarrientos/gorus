import { Routes, Route } from "react-router-dom";
import AccountPage from "../pages/AccountPage";
import HomePage from "../pages/HomePage";
import LoginPage from "../pages/LoginPage";
import RegisterPage from "../pages/RegisterPage";
import ProjectsPage from "../pages/ProjectsPage";
import ProjectPage from "../pages/ProjectPage";
import UsersPage from "../pages/admin/UsersPage";
import NotFoundPage from "../pages/NotFoundPage";
// import Layout from "../components/layouts/Layout";
import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";
import roles from "../helpers/role";
import routes from "../helpers/routes";

export default function AppRouter () {
  return(
    <Routes>
      <Route element={<PublicRoute /> }>
        <Route exact path={routes.login} element={ <LoginPage /> } />
      </Route>
      <Route element={<PublicRoute /> }>
        <Route exact path={routes.register} element={ <RegisterPage /> } />
      </Route>
      <Route element={<PrivateRoute /> }>
        <Route exact path={routes.account} element={ <AccountPage /> } />
      </Route>
      <Route element={<PrivateRoute /> }>
        <Route exact path={routes.projects} element={ <ProjectsPage /> } />
      </Route>
      <Route element={<PrivateRoute /> }>
        <Route exact path={routes.project()} element={ <ProjectPage /> } />
      </Route>
      <Route element={<PrivateRoute hasRole={roles.admin} /> }>
        <Route exact path={routes.admin.users} element={ <UsersPage /> } />
      </Route>
      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  )
}
